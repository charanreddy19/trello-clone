const express=require("express")
const app=express()
const path=require("path")

app.get("/home",(req,res)=>{
    res.sendFile(path.join(__dirname,'home.html'))
})

app.get("/boards",(req,res)=>{
    res.sendFile(path.join(__dirname,"index.html"))
})

app.use(express.static(path.join(__dirname)))



app.listen(8000,()=>{
    console.log("listening")
})