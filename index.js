let key="e4b333731978f610da2a14d6f815fa6c"
let token="79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef"
listId="5fe56f79100ba32a93426d92"
let boardId=localStorage.getItem("boardId")
console.log(boardId)

fetch(`https://api.trello.com/1/boards/${boardId}?key=${key}&token=${token}`, {
  method: 'GET',
  headers: {
    'Accept': 'application/json'
  }
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then((data)=>{
      document.getElementById("navBoardName").innerText=data['name']
  })
  .catch(err => console.error(err));


fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`, {
  method: 'GET'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then((data)=>{
     listId=data[0]['id']
     let card=document.getElementById("card")
let cardSubmit=document.getElementById("cardForm")

let count=1
fetch(`https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`, {
  method: 'GET'
})
  .then(response => {
    return response.json();
  })
  .then(text =>{
      listName()
      for(let i in text){
        count+=1
        let parentElement=document.getElementById("addedList")
        let newDiv=document.createElement("div")
        newDiv.textContent=text[i]['name']
        newDiv.setAttribute("data-id",text[i]['id'])
        newDiv.setAttribute("data-name",text[i]['name'])
        newDiv.setAttribute("data-desc",text[i]['desc'])
        let  newDescription=document.createElement("button")
        newRemove=document.createElement("button")
        newRemove.innerText="remove"
        newRemove.setAttribute("data-id",text[i]['id'])
        newRemove.setAttribute("class","remove")
        newDiv.append(newRemove)
        newDiv.setAttribute("class","cards")
        newDiv.addEventListener("click",(event)=>{
              document.getElementById("description").style.display="flex"
              document.getElementById("descriptionValue").style.border="none"
              document.getElementById("descriptionCardName").innerText=event.target.getAttribute("data-name")
              let id=event.target.getAttribute("data-id")
              document.getElementById("descriptionForm").setAttribute("data-id",id)
              document.getElementsByTagName("BODY")[0].style.opacity=1
              
             document.getElementById("descriptionValue").setAttribute("data-id",event.target.getAttribute("data-desc"))
             document.getElementById("descriptionValue").setAttribute("data-desc",text[i]['desc'])
            document.getElementById("descriptionValue").value=document.getElementById("descriptionValue").getAttribute("data-desc")
            addDescription(event.target)
             deletePreviousChecks()
             getChecklist(id)
          })
        newRemove.addEventListener("click",(event)=>{
            let id=event.target.getAttribute("data-id")
            var answer = window.confirm(`do you want to delete this card?`);
            if (answer) {
                deleteCard(id)
                count-=1
                event.target.parentElement.remove()
            }
            else {
                console.log('non deletable')
            } 
        })
        parentElement.insertBefore(newDiv,parentElement.childNodes[count])
      }
  })
  .catch(err => console.error(err));


cardSubmit.addEventListener("submit",(event)=>{
    event.preventDefault()
    if(card.value.length>0){
        count++
        let parentElement=document.getElementById("addedList")
        newDiv=document.createElement("div")
        newDiv.innerText=card.value
        newDiv.setAttribute("data-name",card.value)
        
        newRemove=document.createElement("button")
        newRemove.innerText="remove"
        newRemove.setAttribute("class","remove")
        newDiv.append(newRemove)
        document.getElementById("descriptionValue").setAttribute("data-desc",'')
        newDiv.addEventListener("click",(event)=>{
              document.getElementById("description").style.display="flex"
              document.getElementById("descriptionValue").style.border="none"
              document.getElementById("descriptionCardName").innerText=event.target.getAttribute("data-name")
              let id=event.target.getAttribute("data-id")
              document.getElementById("descriptionForm").setAttribute("data-id",id)
              let div=event.target
              document.getElementsByTagName("BODY")[0].style.opacity=1
              addDescription(div)
              document.getElementById("descriptionValue").value=document.getElementById("descriptionValue").getAttribute("data-desc")
             deletePreviousChecks()
             getChecklist(id)
          })

        newRemove.addEventListener("click",(event)=>{
            let id=event.target.getAttribute("data-id")
            let answer = window.confirm(`do you want to delete this card?`);
            if (answer) {
                deleteCard(id)
                count-=1
                event.target.parentElement.remove()
            }
            else {
                
            }
        })
        createCard()
        newDiv.setAttribute("class","cards")
        parentElement.insertBefore(newDiv,parentElement.childNodes[count])
        card.value=''
    }
})

let list=document.getElementById("list")
let listSubmit=document.getElementById("formList")
counter=1
listSubmit.addEventListener("submit",(event)=>{
    event.preventDefault()
    if(list.value.length>0){
        counter++
        let parentElement=document.getElementById("lists")
        let newDivInner=document.createElement("h2")
        newDivOuter=document.createElement("div")
        newDivOuter.setAttribute("class","createList")
        newDivInner.innerText=list.value
        let newInput=document.createElement("input")
        newInput.setAttribute("placeHolder","+add another card")
        newInput.setAttribute("type","text")
        newDiv=document.createElement("div")
        newDiv.append(newInput)
        newButton=document.createElement("button")
        newButton.innerText="Add Card"
        newButton.setAttribute("type","button")
        newButton.style.marginLeft="1px"
        newDiv.append(newButton)
        console.log(list.value)
        newDivOuter.append(newDivInner)
        newDivOuter.append(newDiv)
        newDivOuter.style.marginLeft="1vw"
        parentElement.insertBefore(newDivOuter,parentElement.childNodes[counter])
        list.value=''
    }
})


function createCard(){
    fetch(`https://api.trello.com/1/cards?key=${key}&token=${token}&idList=${listId}&name=${card.value}`, {
        method: 'POST'
      })
    .then(response => {
        return response.json();
    })
    .then(text =>{
        newDiv.setAttribute("data-id",text['id'])
        newRemove.setAttribute("data-id",text['id'])
    })
    .catch(err => console.error(err));
}

function deleteCard(id){
    fetch(`https://api.trello.com/1/cards/${id}?key=${key}&token=${token}`, {
    method: 'DELETE'
   })
  .then(response => {
    return response.text();
  })
  .then(text => console.log(text))
  .catch(err => console.error(err));
}



function listName(){
  fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`, {
  method: 'GET',
  headers: {
    'Accept': 'application/json'
  }
  })
  .then(response => {
    return response.json();
  })
  .then(text => {
    document.getElementById("boardName").innerText=text[0]['name']
    document.getElementById("innerDescriptionCardName").innerText=text[0]['name']
  })
  .catch(err => console.error(err));
}


function updateCard(id,description) {
  fetch(`https://api.trello.com/1/cards/${id}?key=${key}&token=${token}&desc=${description}`, {
  method: 'PUT',
  headers: {
    'Accept': 'application/json'
  }
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.text();
  })
  .then(text => console.log(text))
  .catch(err => console.error(err));
}


let close=document.getElementById("close")
close.addEventListener("click",(event)=>{
  document.getElementById("description").style.display="none"
  document.getElementsByTagName("BODY")[0].style.opacity=1
})

function addDescription(div){
  document.getElementById("descriptionForm").addEventListener("submit",(event)=>{
    event.preventDefault()
    let description=document.getElementById("descriptionValue").value
    let id=event.target.getAttribute("data-id")
    updateCard(id,description)
    document.getElementById("descriptionValue").style.border="none"
    document.getElementById("descriptionValue").setAttribute("class","inputDesc")
    document.getElementById("descriptionValue").setAttribute("data-desc",description)
    div.setAttribute("data-desc",description)
  })
}

function editEvent(){
      document.getElementById("editButton").addEventListener("click",(event)=>{
        document.getElementById("descriptionValue").style.border="solid"
    })
}

editEvent()


function displayChecklistDiv() {
  let button=document.getElementById("checkListButton")
  button.addEventListener("click",(event)=>{
    document.getElementById("checklistAdder").style.display="flex"
  })
}

displayChecklistDiv()


function addChecklist() {
  let form=document.getElementById("checklistForm")
  form.addEventListener("submit",(event)=>{
    event.preventDefault()
    document.getElementById("checklistAdder").style.display="none"
    let checklistName=document.getElementById("checklist").value
    createCheckList()
  })
}

function apiCreateChecklist(id,name,deleteButton,addButton){
  fetch(`https://api.trello.com/1/cards/${id}/checklists?key=${key}&token=${token}&name=${name}`, {
  method: 'POST'
  })
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(text => {
    deleteButton.setAttribute("data-id",text['id'])
    addButton.setAttribute("data-id",text['id'])
    addButton.setAttribute("id",text['id'])
  })
  .catch(err => console.error(err));
}

function deletePreviousChecks(){
  let parentElement=document.getElementById("checklistContainer")
  let length=parentElement.childNodes.length
  let elements = document.getElementsByClassName("checkRemove");
  while(elements.length > 0){
      check-=1
      elements[0].parentNode.removeChild(elements[0]);
  }
}

let check=11
function getChecklist(id) {
  fetch(`https://api.trello.com/1/cards/${id}/checklists?key=${key}&token=${token}`, {
  method: 'GET'
  })
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(text => {
    console.log(text)
    for(let i of text){
      check+=1
      console.log(i['name'])
      let div=document.createElement("div")
      let newDiv=document.createElement("div")
      let h3=document.createElement("h3")
      h3.innerText=i['name']
      h3.setAttribute("class","checklistName")
      let newDivTop=document.createElement("div")
      let checklistImg=document.createElement("img")
      checklistImg.setAttribute("src","checklist.svg")
      checklistImg.setAttribute("class","checklistImg")
      checklistImg.setAttribute("id","addedChecklistTopImg")
      let deleteButton=document.createElement("button")
      deleteButton.innerText="delete"
      deleteButton.setAttribute("id","checklistDeleteButton")
      deleteButton.style.width="5vw"
      deleteButton.setAttribute("data-id",i['id'])
      deleteButton.addEventListener("click",(event)=>{
        let answer = window.confirm(`do you want to delete this checkList permanently?`);
        if (answer) {
          check-=1
          let cardId=document.getElementById("descriptionForm").getAttribute("data-id")
          let checkId=event.target.getAttribute("data-id")
          apiDeleteChecklist(cardId,checkId)
          event.target.parentElement.parentElement.remove()    
        }
        else {
            
        } 
      })
      apiGetCheckListItem(i['id'],div)
      let addButton=document.createElement("button")
      addButton.setAttribute("data-id",i['id'])
      addButton.innerText="add an item"
      addButton.style.width="10vw"
      addButton.setAttribute("id",i['id'])
      div.append(newDiv)
      newDivTop.append(checklistImg)
      newDivTop.append(h3)
      newDivTop.setAttribute("class","addedChecklistTopName")
      addCheckItemBar(div)
      div.append(addButton)
      newDiv.append(newDivTop)
      newDiv.append(deleteButton)
      newDiv.setAttribute("class","addedChecklistTop")
      div.setAttribute("class","addedChecklist")
      addButton.addEventListener("click",(event)=>{
        createAddCheckItem(div,i['id'])
        document.getElementById(i['id']).style.display="none"
      })
      let parentElement=document.getElementById("checklistContainer")
     div.setAttribute("class",'checkRemove')
     parentElement.insertBefore(div,parentElement.childNodes[check])
    }
  })
  .catch(err => console.error(err));
}

function createCheckList() {
  check+=1
  let div=document.createElement("div")
  let newDiv=document.createElement("div")
  let h3=document.createElement("h3")
  h3.innerText=document.getElementById("checklist").value
  h3.setAttribute("class","checklistName")
  let newDivTop=document.createElement("div")
  let checklistImg=document.createElement("img")
  checklistImg.setAttribute("src","checklist.svg")
  checklistImg.setAttribute("class","checklistImg")
  checklistImg.setAttribute("id","addedChecklistTopImg")
  let deleteButton=document.createElement("button")
  deleteButton.innerText="delete"
  deleteButton.style.width="5vw"
  deleteButton.setAttribute("id","checklistDeleteButton")
  let addButton=document.createElement("button")
  addButton.innerText="add an item"
  addButton.style.width="10vw"
  div.append(newDiv)
  newDivTop.append(checklistImg)
  newDivTop.append(h3)
  newDivTop.setAttribute("class","addedChecklistTopName")
  addCheckItemBar(div)
  div.append(addButton)
  newDiv.append(newDivTop)
  newDiv.append(deleteButton)
  newDiv.setAttribute("class","addedChecklistTop")
  div.setAttribute("class","addedChecklist")
  let parentElement=document.getElementById("checklistContainer")
 div.setAttribute("class",'checkRemove')
 parentElement.insertBefore(div,parentElement.childNodes[check])

  let id=document.getElementById("descriptionForm").getAttribute("data-id")
    deleteButton.addEventListener("click",(event)=>{
      let answer = window.confirm(`do you want to delete this checkList permanently?`);
      if (answer) {
        check-=1
        event.target.parentElement.parentElement.remove()
        let checkId=event.target.getAttribute("data-id")
        apiDeleteChecklist(id,checkId)   
      }
      else {
          
      } 
    })

  let name=document.getElementById("checklist").value
  apiCreateChecklist(id,name,deleteButton,addButton)
      addButton.addEventListener("click",(event)=>{
        createAddCheckItem(div,event.target.getAttribute("data-id"))
        event.target.style.display="none"
       })
}

addChecklist()


function apiDeleteChecklist(cardId,checkId) {
  fetch(`https://api.trello.com/1/cards/${cardId}/checklists/${checkId}?key=${key}&token=${token}`, {
  method: 'DELETE'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.text();
  })
  .then(text => console.log(text))
  .catch(err => console.error(err));
}


function addCheckItemBar(container) {
 /* let div=document.createElement("div")
  let newDiv=document.createElement("div")
  div.append(newDiv)
  let bar=document.createElement("div")
  div.append(bar)
  newDiv.innerText="0%"
  newDiv.setAttribute("id","check%")
  div.setAttribute("class","checkBarContainer")
  bar.setAttribute("class","checkBar")
  bar.setAttribute("id","checkBar")
  container.append(div)*/
}


function createAddCheckItem(container,id){
  let div=document.createElement("div")
  let newDiv=document.createElement("div")
  let input=document.createElement("input")
  input.setAttribute("type","text")
  input.setAttribute("placeholder","add an item")
  let button=document.createElement("button")
  button.innerText="Add"
  button.addEventListener("click",(event)=>{
     apiAddChecklistItem(id,input.value,container)
  })
  let closeImg=document.createElement("img")
  closeImg.setAttribute("src","close.png")
  closeImg.setAttribute("class","closeItemImg")
  closeImg.setAttribute("data-id2",id)
  closeImg.addEventListener("click",(event)=>{
    document.getElementById(event.target.getAttribute("data-id2")).style.display="block"
    event.target.parentElement.remove()
  })
  div.append(input)
  div.append(button)
  div.append(closeImg)
  div.style.marginTop="1vh"
  div.style.marginLeft="2vw"
  container.insertBefore(div,container.lastChild)
}

function addItem(container,input,id,itemId) {
  let div=document.createElement("div")
  let checkbox=document.createElement("input")
  checkbox.setAttribute("type","checkbox")
  checkbox.style.backgroundColor="black"
  let h4=document.createElement("h4")
  h4.innerText=input
  checkbox.addEventListener("click",(event)=>{
    let cardId=document.getElementById("descriptionForm").getAttribute("data-id")
    if(checkbox.checked===true){
      status="complete"
      updateCheckItem(cardId,id,itemId,status)
      h4.style.textDecoration="line-through"
    }
    else {
        status="incomplete"
        updateCheckItem(cardId,id,itemId,status)
    }
  })
  let newDiv=document.createElement("div")
  div.append(newDiv)
  newDiv.append(checkbox)
  newDiv.setAttribute("class","itemNameAdder")
  newDiv.append(h4)
  let button=document.createElement("button")
  button.innerText="remove"
  button.setAttribute("data-id",itemId)
  button.addEventListener("click",(event)=>{
    event.target.parentElement.remove()
    deleteCheckItem(id,event.target.getAttribute('data-id'))
  })
  div.append(button)
  div.setAttribute("class","itemAdder")
  let length=container.childNodes.length
  container.insertBefore(div,container.childNodes[length-2])
}

function addItem2(container,input,id,checkId,state){
  let div=document.createElement("div")
  let checkbox=document.createElement("input")
  checkbox.setAttribute("type","checkbox")
  checkbox.style.backgroundColor="black"
  let h4=document.createElement("h4")
  h4.innerText=input

  if(state==="incomplete"){
    checkbox.checked=false
  }
  else{
    checkbox.checked=true
    h4.style.textDecoration="line-through"
  }
  checkbox.addEventListener("click",(event)=>{
    let cardId=document.getElementById("descriptionForm").getAttribute("data-id")
  if(checkbox.checked===true){
    status="complete"
    updateCheckItem(cardId,checkId,id,status)
    h4.style.textDecoration="line-through"
  }
  else {
      status="incomplete"
      updateCheckItem(cardId,checkId,id,status)
      h4.style.textDecoration="none"
  }
  })
  let newDiv=document.createElement("div")
  div.append(newDiv)
  newDiv.append(checkbox)
  newDiv.setAttribute("class","itemNameAdder")
  newDiv.append(h4)
  let button=document.createElement("button")
  button.innerText="remove"
  button.setAttribute("data-id",id)
  button.setAttribute("data-checkId",checkId)
  button.addEventListener("click",(event)=>{
    event.target.parentElement.remove()
    deleteCheckItem(event.target.getAttribute('data-checkId'),event.target.getAttribute('data-id'))
  })
  div.append(button)
  div.setAttribute("class","itemAdder")
  let length=container.childNodes.length
  container.insertBefore(div,container.childNodes[length-1])
}

function apiAddChecklistItem(checkId,name,container) {
  fetch(`https://api.trello.com/1/checklists/${checkId}/checkItems?key=${key}&token=${token}&name=${name}`, {
  method: 'POST'
  })
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(text =>{
     console.log(text)
     addItem(container,name,checkId,text['id'])
  })
  .catch(err => console.error(err));
}


function deleteCheckItem(id,idCheckItem) {
  fetch(`https://api.trello.com/1/checklists/${id}/checkItems/${idCheckItem}?key=${key}&token=${token}`, {
  method: 'DELETE'
  })
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.text();
  })
  .then(text => console.log(text))
  .catch(err => console.error(err));
}


function apiGetCheckListItem(id,container) {
  fetch(`https://api.trello.com/1/checklists/${id}/checkItems?key=${key}&token=${token}`, {
  method: 'GET'
  })
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(text => {
    for(let i of text){
      console.log(i['state'])
      addItem2(container,i['name'],i['id'],i['idChecklist'],i['state'])
    }
  })
  .catch(err => console.error(err));
}


function updateCheckItem(cardId,checklistId,checkItemId,status){
    fetch(`https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}?key=${key}&token=${token}&state=${status}`, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json'
        }
    })
    .then(response => {
        console.log(
          `Response: ${response.status} ${response.statusText}`
        );
    return response.text();
    })
    .then(text => console.log(text))
    .catch(err => console.error(err));
}
  })
  .catch(err => console.error(err));
