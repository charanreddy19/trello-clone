function fetchBoards(){
    fetch('https://api.trello.com/1/members/me/boards?fields=name,url&key=e4b333731978f610da2a14d6f815fa6c&token=79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef')
    .then((data)=>{
       return data.json()
    })
    .then((data)=>{
        let div=document.getElementById("board")
        for(let i of data){
          newH2=document.createElement("h2")
          newLink=document.createElement("a")
          newLink.setAttribute("href","index.html")
          newH2.innerText=i['name']
          newH2.setAttribute("data-id",i['id'])
          newLink.append(newH2)
          newLink.addEventListener("click",(event)=>{
              localStorage.setItem("boardId",event.target.getAttribute("data-id"))
          })
          div.append(newLink)
        }
    })
}

fetchBoards()